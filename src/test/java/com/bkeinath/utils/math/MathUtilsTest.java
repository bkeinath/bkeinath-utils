package com.bkeinath.utils.math;

import static com.bkeinath.utils.math.MathUtils.getAsBigDecimal;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class MathUtilsTest {

	@Test
	public void testSqrt() {
		BigDecimal sqrt = MathUtils.sqrt(MathUtils.TWO, 10);
		Assert.assertEquals(sqrt, new BigDecimal("1.4142135624"));
	}

	@Test
	public void testStandardDeviation() {
		//9, 2, 5, 4, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4
		List<BigDecimal> values = Arrays.asList(
				getAsBigDecimal(9)
				, getAsBigDecimal(2)
				, getAsBigDecimal(5)
				, getAsBigDecimal(4)
				, getAsBigDecimal(12)
				, getAsBigDecimal(7)
				, getAsBigDecimal(8)
				, getAsBigDecimal(11)
				, getAsBigDecimal(9)
				, getAsBigDecimal(3)
				, getAsBigDecimal(7)
				, getAsBigDecimal(4)
				, getAsBigDecimal(12)
				, getAsBigDecimal(5)
				, getAsBigDecimal(4)
				, getAsBigDecimal(10)
				, getAsBigDecimal(9)
				, getAsBigDecimal(6)
				, getAsBigDecimal(9)
				, getAsBigDecimal(4)
				);
		BigDecimal std = MathUtils.getStandardDeviation(values);
		Assert.assertEquals(std, new BigDecimal("2.9832867781"));
	}
	
	@Test
	public void testGetAverageClosenessFactor() {
		Double r1 = MathUtils.getAverageClosenessFactor(Arrays.asList(new BigDecimal("50"), new BigDecimal("50.00")));
		System.out.println(r1);
		
		r1 = MathUtils.getAverageClosenessFactor(Arrays.asList(new BigDecimal("40"), new BigDecimal("60")));
		System.out.println(r1);
		
		r1 = MathUtils.getAverageClosenessFactor(Arrays.asList(new BigDecimal("0"), new BigDecimal("100")));
		System.out.println(r1);
		
		r1 = MathUtils.getAverageClosenessFactor(Arrays.asList(new BigDecimal("-10"), new BigDecimal("110")));
		System.out.println(r1);
		
		r1 = MathUtils.getAverageClosenessFactor(Arrays.asList(new BigDecimal("2.1"), new BigDecimal("2.2")));
		System.out.println(r1);
	}
	
}

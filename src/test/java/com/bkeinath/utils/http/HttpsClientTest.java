package com.bkeinath.utils.http;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Ignore;
import org.junit.Test;

public class HttpsClientTest {

	@Test
	@Ignore
	public void test() throws ClientProtocolException, IOException {
        String url = "https://bigcharts.marketwatch.com/quickchart/options.asp?symb=AAPL";
        String response = HttpsClient.makeGetRequest(url);
        System.out.println(response);
	}

}

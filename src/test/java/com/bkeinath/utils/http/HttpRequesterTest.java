package com.bkeinath.utils.http;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.bkeinath.utils.file.FileUtils;

public class HttpRequesterTest {

	@Test
	@Ignore
	public void test() throws FileNotFoundException {
		String url = "https://research.investors.com/options-center/reports/option-volume";
		Map<String, String> params = new HashMap<>();
		//__EVENTTARGET: ctl00$ctl00$ctl00$secondaryContent$leftContent$ReportContent$pagingFilter$ctl01$ctl00
		params.put("__EVENTTARGET", "ctl00$ctl00$ctl00$secondaryContent$leftContent$ReportContent$pagingFilter$ctl01$ctl00");
		//String response = HttpRequester.makePostRequest(url);
		String response = HttpRequester.makeRequest(url, null, params);
		FileUtils.print(Arrays.asList(response), "test.txt");
	}

	@Test
	@Ignore
	public void test2() {
		String url = "https://query1.finance.yahoo.com/v7/finance/options/AAPL?formatted=true&lang=en-US&region=US&corsDomain=finance.yahoo.com";
		
		/*
accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*//*;q=0.8,application/signed-exchange;v=b3
accept-encoding: gzip, deflate, br
accept-language: en-US,en;q=0.9
sec-fetch-mode: navigate
sec-fetch-site: none
upgrade-insecure-requests: 1
user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36
		 */
		
		Map<String, String> requestProperties = HttpRequester.DEFAULT_REQUEST_PROPERTIES;
//		requestProperties.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*//*;q=0.8,application/signed-exchange;v=b3");
//		requestProperties.put("accept-encoding", "gzip, deflate, br");
//		requestProperties.put("accept-language", "en-US,en;q=0.9");
//		requestProperties.put("sec-fetch-mode", "navigate");
//		requestProperties.put("sec-fetch-site", "none");
//		requestProperties.put("upgrade-insecure-requests", "1");
		String response = HttpRequester.makeRequest(url, Collections.emptyMap(), null, requestProperties);
		System.out.println(response);
	}
}

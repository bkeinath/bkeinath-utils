package com.bkeinath.utils.parse;

import org.apache.commons.lang3.StringUtils;

public class ParseUtils {

	private static final String NUMBERS = "0123456789";
	private static final String REGEX_ALPHANUMERIC_ONLY = "[^a-zA-Z0-9]";
	private static final String BLANK = "";
	private static final String ALLOWED_TRADE_BROADCAST_CHARACTERS = "1234567890.ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$- \n";
	
	public static String keepAlphaNumeric(String value) {
		if (StringUtils.isBlank(value)) {
			return value;
		}
		return value.replaceAll(REGEX_ALPHANUMERIC_ONLY, BLANK);
	}
	
	public static String keepNumeric(String value) {
		return clean(value, NUMBERS);
	}
	
	public static String cleanTradeBroadcastMessage(String value) {
		return clean(value, ALLOWED_TRADE_BROADCAST_CHARACTERS);
	}
	
	public static String clean(String value, String allowedValues) {
		if (StringUtils.isBlank(value) || StringUtils.isBlank(allowedValues)) {
			return value;
		}
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < value.length(); i++) {
			String strAtIndex = String.valueOf(value.charAt(i));
			if (allowedValues.contains(strAtIndex)) {
				sb.append(strAtIndex);
			}
		}
		
		return sb.toString();
	}
}

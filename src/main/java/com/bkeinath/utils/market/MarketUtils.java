package com.bkeinath.utils.market;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.bkeinath.utils.file.FileUtils;

public class MarketUtils {

	private static List<String> ETF_LIST = new ArrayList<>();
	
	public static void main(String[] args) {
		boolean b = isEtf("GUSH");
		System.out.println(b);
		b = isEtf("AAPL");
		System.out.println(b);
	}
	
	public static boolean isEtf(String symbol) {
		if (CollectionUtils.isEmpty(ETF_LIST)) {
			String etfs = FileUtils.getFile("etfList.txt");
			if (StringUtils.isNotBlank(etfs)) {
				ETF_LIST = Arrays.asList(etfs.split("\n"));
			}
		}
		return ETF_LIST.contains(symbol.toUpperCase());
	}
}

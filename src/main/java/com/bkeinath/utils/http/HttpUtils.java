package com.bkeinath.utils.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

public class HttpUtils {

	public static String convertMapToParamString(Map<String, String> params, boolean encode) throws UnsupportedEncodingException {
		StringBuilder paramQuery = new StringBuilder();
		if (params != null && params.size() > 0) {
			int i = 0;
			for (Entry<String, String> param : params.entrySet()) {
				if (i > 0) {
					paramQuery.append(HttpConstants.AMPERSAND);
				}
				i++;
				String key = param.getKey();
				String value = param.getValue();
				if (encode) {
					key = URLEncoder.encode(key, HttpConstants.UTF_8);
					value = URLEncoder.encode(value, HttpConstants.UTF_8);
				}
				paramQuery.append(key).append(HttpConstants.EQUALS).append(value);
			}
		}
		return paramQuery.toString();
	}
}

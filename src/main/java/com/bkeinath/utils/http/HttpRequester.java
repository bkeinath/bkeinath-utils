package com.bkeinath.utils.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

public class HttpRequester {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(HttpRequester.class);

	private static final String PROXY_URL = null;
	private static final int PROXY_PORT = 0;
	private static final String PROXY_NETWORK = null;
	private static final String PROXY_USERNAME = null;
	private static final String PROXY_PASSWORD = null;
	public static final Map<String, String> DEFAULT_REQUEST_PROPERTIES;
	
	static {
		DEFAULT_REQUEST_PROPERTIES = new HashMap<>();
		DEFAULT_REQUEST_PROPERTIES.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
	}

	private HttpRequester() {}
	
	private static final HostnameVerifier HOSTNAME_VERIFIER = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};
	
	public static String makeGetRequest(String url) {
		return makeRequest(url, Collections.emptyMap(), null);
	}
	
	public static String makePostRequest(String url) {
		return makeRequest(url, null, Collections.emptyMap());
	}
	
	public static String makeRequest(String url, Map<String, String> getParams, Map<String, String> postParams) {
		return makeRequest(url, getParams, postParams, DEFAULT_REQUEST_PROPERTIES);
	}
	
	public static String makeRequest(String url, Map<String, String> getParams, Map<String, String> postParams, Map<String, String> requestProperties)  {
		String responseText = null;
		HttpURLConnection conn = null;
		
		try {
			conn = processConnection(url, getParams, postParams, requestProperties);
			responseText = getResponse(conn);
		} catch (IOException e) {
			log.error(e.toString(), e);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return responseText;
	}
	
	private static HttpURLConnection processConnection(String baseUrl, Map<String, String> getParams, Map<String, String> postParams, Map<String, String> requestProperties) throws IOException {
		StringBuilder getParamQuery = new StringBuilder(baseUrl);
		boolean getRequest = getParams != null ? true : false;
		if (getParams != null && getParams.size() > 0) {
			getParamQuery.append(HttpConstants.QUESTION_MARK);
			getParamQuery.append(HttpUtils.convertMapToParamString(getParams, true));
		}
		String url = getParamQuery.toString();
		HttpURLConnection con = createConnection(url, getRequest, requestProperties);
		
		// Send request
		if (!getRequest) {
			Writer out = new OutputStreamWriter(con.getOutputStream());
			if (postParams != null && postParams.size() > 0) {
				String postParamStr = HttpUtils.convertMapToParamString(postParams, false);
				out.write(postParamStr);
			}
			out.flush();
			out.close();
		}
		
		return con;
	}
	
	private static HttpURLConnection createConnection(String urlStr, boolean getRequest, Map<String, String> requestProperties) throws IOException {
		URL url = new URL(urlStr);
		Proxy proxy = getProxy(PROXY_USERNAME, PROXY_PASSWORD);
		HttpURLConnection con = null;
		if (proxy != null) {
			con = (HttpURLConnection) url.openConnection(proxy);
		} else {
			con = (HttpURLConnection) url.openConnection();
		}
		
		if (getRequest) {
			con.setRequestMethod("GET");
		}
		
		if (con instanceof HttpsURLConnection) {
			((HttpsURLConnection) con).setHostnameVerifier(HOSTNAME_VERIFIER);
		}
		
		con.setAllowUserInteraction(false);
		con.setDoOutput(!getRequest);
		con.setDoInput(true);
		con.setUseCaches(false);
		con.setConnectTimeout(HttpConstants.CONNECTION_TIMEOUT_MS);
		con.setReadTimeout(HttpConstants.CONNECTION_READ_TIMEOUT_MS);
		if (MapUtils.isNotEmpty(requestProperties)) {
			for (Entry<String, String> e : requestProperties.entrySet()) {
				//con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36");
				con.setRequestProperty(e.getKey(), e.getValue());
			}
		}
		
		con.connect();
		
		return con;
	}
	
	private static String getResponse(HttpURLConnection con) throws IOException {
		StringBuilder response = new StringBuilder();
		BufferedReader in = null;
		String line = null;
		
		try {
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			while ((line = in.readLine()) != null) {
				response.append(line);
			}
		} catch (IOException e) {
			log.error(e.toString(), e);
			return e.toString();
		} finally {
			if (in != null) {
				in.close();
			}
		}
		return response.toString();
	}
	
	private static Proxy getProxy(String networkUsername, String networkPassword) {
		if (StringUtils.isBlank(networkUsername) || StringUtils.isBlank(networkPassword)
				|| StringUtils.isBlank(PROXY_URL) || StringUtils.isBlank(PROXY_NETWORK)) {
			return null;
		}
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_URL, PROXY_PORT));
		Authenticator authenticator = new Authenticator() {
			
			public PasswordAuthentication getPasswordAuthentication() {
				return (new PasswordAuthentication(PROXY_NETWORK + HttpConstants.BACKSLASH + networkUsername,
						networkPassword.toCharArray()));
			}
		};
		Authenticator.setDefault(authenticator);
		return proxy;
	}
	
}

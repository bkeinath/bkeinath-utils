package com.bkeinath.utils.http;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

public class HttpsClient {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(HttpsClient.class);
	
	public static HttpClient getHttpClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		return HttpClients.custom()
				.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build())
				.setRedirectStrategy(new LaxRedirectStrategy())
				.setDefaultCookieStore(new BasicCookieStore())
				.setSSLSocketFactory(new SSLConnectionSocketFactory(
						SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(),
						NoopHostnameVerifier.INSTANCE))
				.build();
	}
	
	public static String makePostRequest(String url, String jsonBody) throws ClientProtocolException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		HttpClient httpClient = getHttpClient(); 
		HttpPost httpPost = new HttpPost(url);

		StringEntity requestEntity = new StringEntity(
				jsonBody,
			    ContentType.APPLICATION_JSON);
		httpPost.setEntity(requestEntity);
		
		HttpResponse response = httpClient.execute(httpPost);
		return EntityUtils.toString(response.getEntity());
	}
	
	public static String makePostRequest(String url, List<NameValuePair> params) throws ClientProtocolException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		return makePostRequest(getHttpClient(), url, params);
	}
	
	public static String makePostRequest(HttpClient httpClient, String url, List<NameValuePair> params) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new UrlEncodedFormEntity(params));
		HttpResponse response = httpClient.execute(httpPost);
		return EntityUtils.toString(response.getEntity());
	}
	
	public static String makeGetRequest(String url) {
		HttpClient httpClient = HttpClients.custom()
		        .setDefaultRequestConfig(RequestConfig.custom()
		            .setCookieSpec(CookieSpecs.STANDARD).build())
		        .build();
        HttpGet getMethod = new HttpGet(url);
        HttpResponse response;
		try {
			response = httpClient.execute(getMethod);
			return EntityUtils.toString(response.getEntity());
			//return getResponseText(response);
		} catch (IOException e) {
			log.error(e.toString(), e);
		}
		return null;
	}
	
	/*
	private static String getResponseText(HttpResponse response) throws IOException {
		StringBuilder sb = new StringBuilder();
		if (response != null) {
			BufferedReader br = null;
			try {
				InputStream is = response.getEntity().getContent();
				Reader isReader = new InputStreamReader(is);
				br = new BufferedReader(isReader);
				
				String line = br.readLine();
				
				while (line != null) {
					sb.append(line);
					line = br.readLine();
				}
			} finally {
				if (br != null) {
					br.close();
				}
				EntityUtils.consumeQuietly(response.getEntity());
			}
		}
		return sb.toString();
	}
	*/

}

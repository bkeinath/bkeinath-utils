package com.bkeinath.utils.http;

public abstract class HttpConstants {

	public static final String QUESTION_MARK = "?";
	public static final String AMPERSAND = "&";
	public static final String EQUALS = "=";
	public static final String BACKSLASH = "\\";
	public static final String UTF_8 = "UTF-8";
	public static final int CONNECTION_TIMEOUT_MS = 10000;
	public static final int CONNECTION_READ_TIMEOUT_MS = 10000;
	
}

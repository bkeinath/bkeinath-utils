package com.bkeinath.utils.excel;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bkeinath.utils.excel.domain.ExcelSpreadsheetData;
import com.bkeinath.utils.excel.domain.ExcelSpreadsheetRow;

public class ExcelSpreadsheetReader {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(ExcelSpreadsheetReader.class);
	private static final String BLANK = "";
	private static final String ERROR = "eRRoR";
	
	public static Workbook getWorkbookFromUrl(String urlStr) {
		Workbook workbook = null;
		try {
			URL url = new URL(urlStr);
			URLConnection uc = url.openConnection();
			workbook = getWorkbook(uc.getInputStream(), urlStr);
		} catch (IOException e) {
			log.debug("Failed to get Workbook from URL. [" + urlStr + "]");
		}
		return workbook;
	}
	
	public static Workbook getWorkbook(InputStream is, String fileName) {
		Workbook workbook = null;
		try {
			if (fileName.endsWith(".xlsx")) {
				workbook = new XSSFWorkbook(is);
			} else {
				workbook = new HSSFWorkbook(is);
			}
		} catch (IOException e) {
			log.debug("Failed to get Workbook from [" + fileName + "]");
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error(e.toString(), e);
				}
			}
		}
		return workbook;
	}
	
	//used for debugging
	public static void printSpreadsheetData(Map<Integer, ExcelSpreadsheetData> data) {
		for (Entry<Integer, ExcelSpreadsheetData> entry : data.entrySet()) {
			System.out.println("Entry: " + entry.getKey());
			for (ExcelSpreadsheetRow row : entry.getValue().getRowValues()) {
				System.out.println(row.getRowData().toString());
			}
		}
	}
	
	public static ExcelSpreadsheetData getSpreadsheetDataFirstPage(InputStream is, String fileName) {
		Map<Integer, ExcelSpreadsheetData> map = getSpreadsheetData(is, fileName);
		if (MapUtils.isEmpty(map)) {
			return null;
		}
		return map.values().iterator().next();
	}
	
	public static Map<Integer, ExcelSpreadsheetData> getSpreadsheetData(InputStream is, String fileName) {
		Workbook workbook = getWorkbook(is, fileName);
		if (workbook == null) {
			return null;
		}
		return getSpreadsheetData(workbook);
	}

	public static Map<Integer, ExcelSpreadsheetData> getSpreadsheetData(Workbook workbook) {
		return getSpreadsheetData(workbook, null);
	}
		
	public static Map<Integer, ExcelSpreadsheetData> getSpreadsheetData(Workbook workbook, String firstColumnHeader) {	
		Map<Integer, ExcelSpreadsheetData> sheetDataMap = new HashMap<>();
		
		if (workbook == null) {
			return sheetDataMap;
		}
		
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			if (sheet != null) {
				Iterator<Row> rows = sheet.rowIterator();
				if (rows != null) {
					ExcelSpreadsheetData data = new ExcelSpreadsheetData();
					List<ExcelSpreadsheetRow> rowList = null;
			
					while (rows.hasNext()) {
						Row row = rows.next();
						ExcelSpreadsheetRow ssRow = new ExcelSpreadsheetRow();
						List<String> rowValues = getRowValues(row);
						
						if (data.getColumnHeaders().isEmpty() && 
								(StringUtils.isBlank(firstColumnHeader) || rowValues.get(0).equalsIgnoreCase(firstColumnHeader))) {
							data.setColumnHeaders(rowValues);
							rowList = new ArrayList<>();
						}
						
						if (rowList == null) {
							//haven't gotten column headers yet
							continue;
						}

						ssRow.setRowData(rowValues);
						rowList.add(ssRow);
					}
					data.setRowValues(rowList != null ? rowList : Collections.emptyList());
					sheetDataMap.put(Integer.valueOf(i), data);
				}
			}
		}
		
		return sheetDataMap;
	}
	
	private static List<String> getRowValues(Row row) {
		List<String> rowList = new ArrayList<>();
		
		for (int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++) {
			String cellValue = BLANK;
			
			Cell cell = row.getCell(cellIndex, MissingCellPolicy.RETURN_NULL_AND_BLANK);

			if (cell != null) {
				switch (cell.getCellTypeEnum()) {
					case BOOLEAN:
						cellValue = String.valueOf(cell.getBooleanCellValue());
						break;
					case ERROR:
						cellValue = ERROR;
						break;
					case FORMULA:
						switch (cell.getCachedFormulaResultTypeEnum()) {
						case NUMERIC:
							cellValue = getNumericCellValue(cell);
							break;
						case STRING:
							cellValue = getStringCellValue(cell);
							break;
						case BOOLEAN:
							cellValue = String.valueOf(cell.getBooleanCellValue());
							break;
						case ERROR:
							cellValue = ERROR;
							break;
						case BLANK:
						case _NONE:
						default:
							cellValue = BLANK;
							break;
						}
						break;
					case NUMERIC:
						cellValue = getNumericCellValue(cell);
						break;
					case STRING:
						cellValue = getStringCellValue(cell);
						break;
					case BLANK:
					case _NONE:
					default:
						cellValue = BLANK;
						break;
				}
			}
			rowList.add(cellValue.trim());
		}
		return rowList;
	}
	
	private static String getNumericCellValue(Cell cell) {
		String cellValue = String.valueOf(cell.getNumericCellValue());
		String[] numericTokens = cellValue.split("\\.");
		String afterPeriod = null;
		if (numericTokens != null && numericTokens.length > 0) {
			cellValue = numericTokens[0];
		}
		if (numericTokens != null && numericTokens.length > 1) {
			afterPeriod = numericTokens[1];
		}
		if (afterPeriod != null && (afterPeriod.contains("E") || afterPeriod.contains("e"))) {
			BigDecimal bd = new BigDecimal(cell.getNumericCellValue());
			cellValue = bd.toPlainString();
		}
		return cellValue;
	}
	
	private static String getStringCellValue(Cell cell) {
		RichTextString richTextString = cell.getRichStringCellValue();
		return richTextString.toString();
	}
}

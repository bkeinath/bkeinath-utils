package com.bkeinath.utils.excel.domain;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ExcelSpreadsheetRow {

	private List<String> rowData;

	public ExcelSpreadsheetRow() {
	}
	
	public ExcelSpreadsheetRow(List<String> rowData) {
		this.rowData = rowData;
	}
	
	public List<String> getRowData() {
		return rowData;
	}

	public void setRowData(List<String> rowData) {
		this.rowData = rowData;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("rowData", rowData == null ? "" : rowData)
				.toString();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(
				rowData
				);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !this.getClass().equals(obj.getClass())) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		
		final ExcelSpreadsheetRow other = (ExcelSpreadsheetRow) obj;
		return Objects.equals(this.rowData, other.rowData)
				;
	}
}

package com.bkeinath.utils.excel.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ExcelSpreadsheetData {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(ExcelSpreadsheetData.class);
	private static final String SPACE = " ";
	private static final String NEW_LINE = "\n";
	public static final String BLANK = "";
	private List<String> columnHeaders = new ArrayList<>();
	private List<ExcelSpreadsheetRow> rowValues = new ArrayList<>();
	private Map<String, Integer> columnHeaderIndexMap = new HashMap<>();
	
	public List<String> getColumnHeaders() {
		return columnHeaders;
	}

	public void setColumnHeaders(List<String> columnHeaders) {
		List<String> cleanedList = new ArrayList<>();
		for (String header : columnHeaders) {
			cleanedList.add(clean(header));
		}
		this.columnHeaders = cleanedList;
	}
	
	public void addColumnHeader(String columnHeader) {
		if (StringUtils.isNotBlank(columnHeader)) {
			this.columnHeaders.add(clean(columnHeader));
		}
	}

	public List<ExcelSpreadsheetRow> getRowValues() {
		return rowValues;
	}

	public void setRowValues(List<ExcelSpreadsheetRow> rowValues) {
		this.rowValues = rowValues;
	}
	
	public void addRowValue(ExcelSpreadsheetRow rowValue) {
		this.rowValues.add(rowValue);
	}
	
	public void addRowValues(List<ExcelSpreadsheetRow> rowValues) {
		this.rowValues.addAll(rowValues);
	}
	
	public int getNumberOfRows() {
		return this.rowValues.size();
	}

	public Map<String, Integer> getColumnHeaderIndexMap() {
		return columnHeaderIndexMap;
	}

	public void setColumnHeaderIndexMap(Map<String, Integer> columnHeaderIndexMap) {
		this.columnHeaderIndexMap = columnHeaderIndexMap;
	}
	
	private String clean(String str) {
		if (str == null) {
			str = BLANK;
		}
		str = str.replace(NEW_LINE, SPACE);
		return str;
	}
	
	public String getValueForColumn(int row, String columnHeaderName) {
		String value = BLANK;
		Integer columnIndex = null;
		
		if (StringUtils.isBlank(columnHeaderName)) {
			return value;
		}
		
		if (columnHeaderIndexMap.size() == 0) {
			for (int i = 0; i < columnHeaders.size(); i++) {
				columnHeaderIndexMap.put(columnHeaders.get(i).toUpperCase(), Integer.valueOf(i));
			}
		}
		
		Integer integer = columnHeaderIndexMap.get(columnHeaderName.toUpperCase());
		if (integer != null) {
			columnIndex = integer;
		}
		
		if (columnIndex != null && row >= 0) {
			ExcelSpreadsheetRow ssRow = null;
			try {
				ssRow = rowValues.get(row);
			} catch (IndexOutOfBoundsException e) {
				log.warn(e.toString(),e);
			}
			if (ssRow != null) {
				try {
					value = ssRow.getRowData().get(columnIndex);
					if (value != null) {
						value = value.trim();
					}
				} catch (Exception e) {
					//ignore as columns without values will be treated as blank.
				}
			}
		}
		return value;
	}
	
	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		builder.append("columnHeaders", columnHeaders);
		
		for (int i = 0; i < rowValues.size(); i++) {
			builder.append("row " + i, rowValues.get(i));
			builder.append(NEW_LINE);
		}
		
		return builder.toString();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(
				columnHeaders
				,rowValues
				,columnHeaderIndexMap
				);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !this.getClass().equals(obj.getClass())) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		
		final ExcelSpreadsheetData other = (ExcelSpreadsheetData) obj;
		return Objects.equals(this.columnHeaders, other.columnHeaders)
				&& Objects.equals(this.rowValues, other.rowValues)
				&& Objects.equals(this.columnHeaderIndexMap, other.columnHeaderIndexMap)
				;
	}
}

package com.bkeinath.utils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.bkeinath.utils.file.FileUtils;

public class Utils {

	private static final String MY_USER_REF_ID = "S628DVNIOG";

	public static boolean isLocalServer() {
		return StringUtils.isNotBlank(System.getProperty("local"));
	}
	
	public static String getAdminUserRefId() {
		return MY_USER_REF_ID;
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValueAscending(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValueDescending(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
	public static void printMap(Map<?,?> map, String label) {
		printMap(map, label, null, false);
	}
	
	public static String getMapAsString(Map<?,?> map, String label) {
		return printMap(map, label, null, true);
	}
	
	public static String printMap(Map<?,?> map, String label, String filePrintName, boolean toString) {
		StringBuilder sb = new StringBuilder();
		if (map == null) {
			if (toString) {
				sb.append(label + " is null.").append("\n");
			} else {
				System.out.println(label + " is null.");
			}
		}
		List<String> rowsToPrint = null;
		if (StringUtils.isNotBlank(filePrintName)) {
			rowsToPrint = new ArrayList<>();
		}
		
		if (toString) {
			sb.append(label + " - size: " + map.size()).append("\n");
		} else {
			System.out.println(label + " - size: " + map.size());
		}
		
		for (Entry<?,?> entry : map.entrySet()) {
			String str = "\t" + entry.getKey() + "\t-\t" + entry.getValue();
			
			if (toString) {
				sb.append(str).append("\n");	
			} else {
				System.out.println("\t" + entry.getKey() + "\t-\t" + entry.getValue());
			}
			
			if (rowsToPrint != null) {
				rowsToPrint.add(str);
			}
		}
		if (rowsToPrint != null) {
			try {
				FileUtils.print(rowsToPrint, filePrintName);
			} catch (FileNotFoundException e) {
			}
		}
		return sb.toString();
	}
	
	public static void printToFile(String fullFilePath, String contents) {
		try (
				PrintWriter writer = new PrintWriter(fullFilePath, "UTF-8");
			)
		{
			writer.println(contents);
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
		}
	}
}

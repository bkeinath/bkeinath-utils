package com.bkeinath.utils.math;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.bkeinath.utils.format.BigDecimalFormatter;

public class MathUtils {

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(MathUtils.class);
	
	public static final BigDecimal NEGATIVE_ONE_BILLION = new BigDecimal("-1000000000.00");
	public static final BigDecimal NEGATIVE_ONE = new BigDecimal("-1.00");
	public static final BigDecimal ZERO = new BigDecimal("0.00");
	public static final BigDecimal ONE_CENT = new BigDecimal("0.01");
	public static final BigDecimal TWO_CENTS = new BigDecimal("0.02");
	public static final BigDecimal FIVE_CENTS = new BigDecimal("0.05");
	public static final BigDecimal POINT_ZERO_NINE = new BigDecimal("0.09");
	public static final BigDecimal TEN_CENTS = new BigDecimal("0.10");
	public static final BigDecimal FIFTEEN_CENTS = new BigDecimal("0.15");
	public static final BigDecimal TWENTY_CENTS = new BigDecimal("0.20");
	public static final BigDecimal TWENTY_FIVE_CENTS = new BigDecimal("0.25");
	public static final BigDecimal THREE_TENTHS = new BigDecimal("0.30");
	public static final BigDecimal POINT_THIRTY_EIGHT = new BigDecimal("0.38");
	public static final BigDecimal FORTY_CENTS = new BigDecimal("0.40");
	public static final BigDecimal HALF = new BigDecimal("0.50");
	public static final BigDecimal THREE_FORTHS = new BigDecimal("0.75");
	public static final BigDecimal ONE = new BigDecimal("1.00");
	public static final BigDecimal ONE_POINT_ONE = new BigDecimal("1.10");
	public static final BigDecimal ONE_POINT_FOUR = new BigDecimal("1.40");
	public static final BigDecimal ONE_POINT_FIVE = new BigDecimal("1.50");
	public static final BigDecimal TWO = new BigDecimal("2.00");
	public static final BigDecimal TWO_HALF = new BigDecimal("2.50");
	public static final BigDecimal THREE = new BigDecimal("3.00");
	public static final BigDecimal THREE_POINT_ONE_TWO_FIVE = new BigDecimal(3.125);
	public static final BigDecimal THREE_HALF = new BigDecimal("3.50");
	public static final BigDecimal FOUR = new BigDecimal("4.00");
	public static final BigDecimal FOUR_HALF = new BigDecimal("4.50");
	public static final BigDecimal FIVE = new BigDecimal("5.00");
	public static final BigDecimal FIVE_HALF = new BigDecimal("5.50");
	public static final BigDecimal SIX = new BigDecimal("6.00");
	public static final BigDecimal SIX_HALF = new BigDecimal("6.50");
	public static final BigDecimal SEVEN = new BigDecimal("7.00");
	public static final BigDecimal SEVEN_HALF = new BigDecimal("7.50");
	public static final BigDecimal EIGHT = new BigDecimal("8.00");
	public static final BigDecimal EIGHT_HALF = new BigDecimal("8.50");
	public static final BigDecimal NINE = new BigDecimal("9.00");
	public static final BigDecimal NINE_HALF = new BigDecimal("9.50");
	public static final BigDecimal TEN = new BigDecimal("10.00");
	public static final BigDecimal TEN_HALF = new BigDecimal("10.50");
	public static final BigDecimal ELEVEN = new BigDecimal("11.00");
	public static final BigDecimal THIRTEEN = new BigDecimal("13.00");
	public static final BigDecimal FIFTEEN = new BigDecimal("15.00");
	public static final BigDecimal EIGHTEEN = new BigDecimal("18.00");
	public static final BigDecimal TWENTY = new BigDecimal("20.00");
	public static final BigDecimal THIRTY = new BigDecimal("30.00");
	public static final BigDecimal THIRTY_TWO = new BigDecimal("32.00");
	public static final BigDecimal THIRTY_FIVE = new BigDecimal("35.00");
	public static final BigDecimal FORTY = new BigDecimal("40.00");
	public static final BigDecimal FORTY_FIVE = new BigDecimal("45.00");
	public static final BigDecimal FIFTY = new BigDecimal("50.00");
	public static final BigDecimal FIFTY_FIVE = new BigDecimal("55.00");
	public static final BigDecimal SIXTY = new BigDecimal("60.00");
	public static final BigDecimal SIXTY_FIVE = new BigDecimal("65.00");
	public static final BigDecimal SEVENTY = new BigDecimal("70.00");
	public static final BigDecimal SEVENTY_FIVE = new BigDecimal("75.00");
	public static final BigDecimal EIGHTY = new BigDecimal("80.00");
	public static final BigDecimal EIGHTY_FIVE = new BigDecimal("85.00");
	public static final BigDecimal EIGHTY_EIGHT = new BigDecimal("88.00");
	public static final BigDecimal NINTY = new BigDecimal("90.00");
	public static final BigDecimal NINTY_NINE = new BigDecimal("99");
	public static final BigDecimal ONE_HUNDRED = new BigDecimal("100.00");
	public static final BigDecimal ONE_HUNDRED_FIFTY = new BigDecimal("150.00");
	public static final BigDecimal TWO_HUNDRED = new BigDecimal("200.00");
	public static final BigDecimal THREE_HUNDRED = new BigDecimal("300.00");
	public static final BigDecimal FIVE_HUNDRED = new BigDecimal("500.00");
	public static final BigDecimal ONE_THOUSAND = new BigDecimal("1000.00");
	public static final BigDecimal TEN_THOUSAND = new BigDecimal("10000.00");
	public static final BigDecimal TWO_HUNDRED_THOUSAND = new BigDecimal("200000.00");
	public static final BigDecimal ONE_MILLION = new BigDecimal("1000000.00");
	public static final BigDecimal ONE_BILLION = new BigDecimal("1000000000.00");
	public static final BigDecimal ONE_TRILLION = new BigDecimal("1000000000000.00");
	public static final BigDecimal DAYS_PER_YEAR = new BigDecimal("365.25");
	public static final BigDecimal DAYS_2_YEARS = DAYS_PER_YEAR.multiply(TWO);
	public static final BigDecimal DAYS_5_YEARS = DAYS_PER_YEAR.multiply(FIVE);
	public static final BigDecimal DAYS_10_YEARS = DAYS_PER_YEAR.multiply(TEN);
	public static final List<BigDecimal> DAYS_YEARS_LIST = Arrays.asList(DAYS_PER_YEAR, DAYS_2_YEARS, DAYS_5_YEARS, DAYS_10_YEARS);
	public static final int DEFAULT_DECIMAL_PLACES = 2;
	public static final int QUARTERS_IN_A_YEAR = 4;
	public static final BigDecimal QUARTERS_IN_A_YEAR_BD = new BigDecimal(QUARTERS_IN_A_YEAR);

	public static BigDecimal getPercentile(BigDecimal low, BigDecimal high, BigDecimal value) {
		BigDecimal rank = NEGATIVE_ONE;
		if (value != null && low != null && high != null) {
			BigDecimal priceFromLow = value.subtract(low);
			BigDecimal totalDifference = high.subtract(low);
			if (totalDifference.doubleValue() > 0) {
				rank = divide(priceFromLow, totalDifference);
				rank = rank.multiply(ONE_HUNDRED);
			}
		}
		return rank.setScale(2, RoundingMode.CEILING);
	}
	
	public static BigDecimal divide(int q1, int q2) {
		return divide(new BigDecimal(q1), q2);
	}

	public static BigDecimal divide(BigDecimal q1, int q2) {
		return divide(q1, q2, DEFAULT_DECIMAL_PLACES);
	}
	
	public static BigDecimal divide(int q1, BigDecimal q2, int decimalPlaces) {
		return divide(new BigDecimal(q1), q2, decimalPlaces);
	}

	public static BigDecimal divide(BigDecimal q1, int q2, int decimalPlaces) {
		return divide(q1, new BigDecimal(q2), decimalPlaces);
	}

	public static BigDecimal divide(BigDecimal q1, BigDecimal q2) {
		return divide(q1, q2, DEFAULT_DECIMAL_PLACES);
	}

	public static BigDecimal divide(BigDecimal q1, BigDecimal q2, int decimalPlaces) {
		if (isZero(q2)) {
			q2 = NEGATIVE_ONE_BILLION;
		}
		return q1.divide(q2, decimalPlaces, RoundingMode.HALF_UP);
	}

	public static boolean isZero(BigDecimal val) {
		boolean result = false;
		try {
			result = val.compareTo(BigDecimal.ZERO) == 0;
		} catch (Exception e) {
			//ignore
		}
		return result;
	}
	
	public static BigDecimal getPercentChange(BigDecimal newValue, BigDecimal originalValue) {
		BigDecimal diff = newValue.subtract(originalValue);
		return divide(diff, originalValue, 10).multiply(ONE_HUNDRED);
	}

	public static BigDecimal getPercentDifference(BigDecimal newValue, BigDecimal originalValue) {
		return abs(getPercentChange(newValue, originalValue));
	}
	
	public static BigDecimal getDifferenceAbs(BigDecimal value1, BigDecimal value2, int numOfDecimals) {
		return BigDecimalFormatter.setDecimalDigits(abs(value1.subtract(value2)), numOfDecimals);
	}
	
	public static BigDecimal average(List<BigDecimal> list) {
		return average(list, DEFAULT_DECIMAL_PLACES);
	}
	
	public static BigDecimal averageInts(List<Integer> list, int decimalPlaces) {
		return average(getAsBds(list), decimalPlaces);
	}

	public static BigDecimal average(List<BigDecimal> list, int decimalPlaces) {
		BigDecimal average = ZERO;
		BigDecimal sum = ZERO;
		if (list != null) {
			for (BigDecimal bd : list) {
				sum = sum.add(bd);
			}
			average = divide(sum, list.size(), decimalPlaces);
		}
		return average;
	}
	
	public static BigDecimal median(List<BigDecimal> list) {
		Collections.sort(list);
		BigDecimal median = ZERO;
		if (CollectionUtils.isNotEmpty(list)) {
			int size = list.size();
			if (size % 2 == 0) {
				//size is even
				int half = size / 2;
				median = average(Arrays.asList(list.get(half), list.get(half - 1)));
			} else {
				//size is odd
				median = list.get(size / 2);
			}
		}
		return median;
	}

	public static boolean isNumberPositive(BigDecimal value) {
		boolean result = false;
		if (getSign(value) >= 0) {
			result = true;
		}
		return result;
	}
	
	public static boolean isNumberNegative(BigDecimal value) {
		return !isNumberPositive(value);
	}

	public static BigDecimal abs(BigDecimal value) {
		if (!isNumberPositive(value)) {
			value = value.multiply(NEGATIVE_ONE);
		}
		return BigDecimalFormatter.setTwoDecimalDigits(value);
	}

	private static int getSign(BigDecimal value) {
		return value.compareTo(ZERO);
	}

	public static BigDecimal compare(BigDecimal bd1, BigDecimal bd2, boolean getLarger) {
		if (bd1 == null && bd2 != null) {
			return bd2;
		} else if (bd2 == null && bd1 != null) {
			return bd1;
		} else if (bd2 == null && bd1 == null) {
			return null;
		} else if (bd1.compareTo(bd2) < 0 && getLarger) {
			return bd2;
		} else if (bd1.compareTo(bd2) == 0 && getLarger) {
			return null;
		} else if (bd1.compareTo(bd2) > 0 && getLarger) {
			return bd1;
		} else if (bd1.compareTo(bd2) > 0 && !getLarger) {
			return bd2;
		} else if (bd1.compareTo(bd2) < 0 && !getLarger) {
			return bd1;
		} else if (bd1.compareTo(bd2) == 0 && !getLarger) {
			return null;
		}
		log.warn("Comparing 2 null values.  Returning null.");
		return null;
	}
	
	public static boolean isEqual(BigDecimal bd1, BigDecimal bd2) {
		return compare(bd1, bd2, true) == null;
	}

	public static boolean isGreater(BigDecimal bd1, BigDecimal bd2) {
		return bd1 == compare(bd1, bd2, true);
	}

	public static boolean isLess(BigDecimal bd1, BigDecimal bd2) {
		return bd1 == compare(bd1, bd2, false);
	}
	
	public static boolean isGreaterThanOrEqualTo(BigDecimal bd1, BigDecimal bd2) {
		return !isLess(bd1, bd2);
	}
	
	public static boolean isLessThanOrEqualTo(BigDecimal bd1, BigDecimal bd2) {
		return !isGreater(bd1, bd2);
	}
	
	public static boolean isNullOrLessThanOrEqualToZero(BigDecimal bd) {
		return bd == null || isLessThanOrEqualTo(bd, ZERO);
	}
	
	public static boolean isNotNullAndGreaterThanZero(BigDecimal bd) {
		return bd != null && isGreater(bd, ZERO);
	}

	public static BigDecimal roundToNearestHalf(BigDecimal value) {
		BigDecimal divided = value.divide(HALF, 0, RoundingMode.HALF_UP);
		BigDecimal result = divided.multiply(HALF);
		return result;
	}
	
	public static BigDecimal roundToNearestAdjustedHalf(BigDecimal value, boolean adjustHigher) {
		BigDecimal divided = value.divide(HALF, 0, adjustHigher ? RoundingMode.UP : RoundingMode.DOWN);
		BigDecimal result = divided.multiply(HALF);
		return result;
	}
	
	public static String getShortStringDisplay(long value) {
		//805 259 313 152
		if (value < 1000000l) {
			return String.valueOf(value);
		} else {
			BigDecimal bd = new BigDecimal(value);
		
			if (value < 1000000000l) {
				//if less than a billion, divide by a million
				bd = MathUtils.divide(bd, ONE_MILLION);
				return bd.toString() + "M";
			} else if (value < 1000000000000l) {
				//if less than a trillion, divide by a billion
				bd = MathUtils.divide(bd, ONE_BILLION);
				return bd.toString() + "B";
			} else {
				bd = MathUtils.divide(bd, ONE_TRILLION);
				return bd.toString() + "T";
			}
		}
	}
	
	public static boolean isIntegerValue(BigDecimal bd) {
		 return bd.signum() == 0 || bd.scale() <= 0 || bd.stripTrailingZeros().scale() <= 0;
	}
	
	public static BigDecimal getCompoundAnnualGrowthRate(BigDecimal totalWeightedReturn, int totalDays) {
		if (totalWeightedReturn == null || totalDays < 1) {
			return MathUtils.ZERO;
		}
		//[(1 + TWR) ^ (1 / No. of years)] � 1
		BigDecimal cagr = totalWeightedReturn.add(MathUtils.ONE);
		BigDecimal numOfYears = MathUtils.divide(new BigDecimal(totalDays), MathUtils.DAYS_PER_YEAR, 6);
		numOfYears = MathUtils.divide(MathUtils.ONE, numOfYears, 6);
		try {
			double val = Math.pow(cagr.doubleValue(), numOfYears.doubleValue());
			cagr = new BigDecimal(val);
		} catch (Exception e) {
			if (!MathUtils.isNumberPositive(cagr)) {
				double val = Math.pow(MathUtils.abs(cagr).doubleValue(), numOfYears.doubleValue());
				cagr = new BigDecimal(val);
				cagr = cagr.multiply(MathUtils.NEGATIVE_ONE);
			}
		}

		return BigDecimalFormatter.setTwoDecimalDigits(cagr.subtract(MathUtils.ONE).multiply(MathUtils.ONE_HUNDRED));
	}
	
	public static BigDecimal getHpr(BigDecimal startingBalance, BigDecimal endingBalance, BigDecimal cashFlowValue) {
		//(Ending Value / (starting value + cashFlowValue)) - 1
		if (startingBalance == null) {
			startingBalance = MathUtils.ZERO;
		}
		if (cashFlowValue != null) {
			startingBalance = startingBalance.add(cashFlowValue);
		}
		if (endingBalance == null) {
			endingBalance = MathUtils.ZERO;
		}
		BigDecimal hpr = MathUtils.divide(endingBalance, startingBalance, 6);
		hpr = hpr.subtract(MathUtils.ONE);
		return hpr;
	}

	public static boolean isEven(int num) {
		BigDecimal halfBd = MathUtils.divide(num, 2);
		boolean even = false;
		try {
			halfBd.intValueExact();
			even = true;
		} catch (ArithmeticException e) {
			even = false;
		}
		return even;
	}
	
	public static Double getAverageClosenessFactorInts(List<Integer> values) {
		List<BigDecimal> bdValues = getAsBds(values);
		return getAverageClosenessFactor(bdValues);
	}
	
	private static List<BigDecimal> getAsBds(List<Integer> values) {
		List<BigDecimal> bdValues = new ArrayList<>();
		for (Integer v : values) {
			bdValues.add(getAsBigDecimal(v));
		}
		return bdValues;
	}
	
	public static BigDecimal getAsBigDecimal(int n) {
		return new BigDecimal(n);
	}
	
	//The thought here is to return a value that is larger the closer the numbers are together
	//so take the mean and divide by the standard deviation would seem to do this.
	public static Double getAverageClosenessFactor(List<BigDecimal> values) {
		BigDecimal avg = MathUtils.average(values);
		BigDecimal stdDev = getStandardDeviation(values);
		if (MathUtils.isEqual(stdDev, MathUtils.ZERO)) {
			return Double.POSITIVE_INFINITY;
		}
		return MathUtils.divide(avg, stdDev).multiply(avg).doubleValue();
	}
	
	//https://www.mathsisfun.com/data/standard-deviation-formulas.html
	public static BigDecimal getStandardDeviation(List<BigDecimal> values) {
		//step 1 - calc mean
		BigDecimal avg = MathUtils.average(values);
		
		//step 2 & 3 - for each value, subtract mean and square and then get mean of resulting values
		List<BigDecimal> step2Values = new ArrayList<>();
		for (BigDecimal v : values) {
			BigDecimal s = v.subtract(avg);
			s = s.multiply(s);
			step2Values.add(s);
		}
		
		BigDecimal step2ValuesAvg = MathUtils.average(step2Values, 10);
		
		//step 4 - take square root
		BigDecimal step2ValuesAvgSqrt = sqrt(step2ValuesAvg, 10);
		return step2ValuesAvgSqrt;
	}
	
	public static BigDecimal sqrt(BigDecimal value, final int scale) {
	    BigDecimal x0 = BigDecimal.ZERO;
	    BigDecimal x1 = new BigDecimal(Math.sqrt(value.doubleValue()));
	    int i = 0;
	    while (!x0.equals(x1)) {
	        x0 = x1;
	        x1 = MathUtils.divide(value, x0, scale);
	        x1 = x1.add(x0);
	        x1 = MathUtils.divide(x1, MathUtils.TWO, scale);
	        i++;
	        if (i > 100) {
	        	break;
	        }
	    }
	    return x1;
	}

}

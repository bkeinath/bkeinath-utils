package com.bkeinath.utils.random;

import java.util.Random;

public class RandomStringGenerator {

	private static final String VALUES = "ABCDEFGHJKMNOPQRSTUVWXYZ0123456789";
	private static final int LENGTH = 10;
	
	public static String createString() {
		return createString(LENGTH);
	}
	
	public static String createString(int length) {
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(VALUES.charAt(r.nextInt(VALUES.length())));
		}
		return sb.toString();
	}
}

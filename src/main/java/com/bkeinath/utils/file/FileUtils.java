package com.bkeinath.utils.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class FileUtils {

	public static String getFile(String fileName) {
		StringBuilder result = new StringBuilder();

		ClassLoader classLoader = FileUtils.class.getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}
	
	public static <T> void print(List<T> values, String fileName) throws FileNotFoundException {
		if (values == null || StringUtils.isBlank(fileName)) {
			return;
		}
		try (
				PrintStream ps = new PrintStream(new FileOutputStream(fileName));
			) 
		{
			for (T v : values) {
				ps.println(v.toString());
			}
		}
	}
}

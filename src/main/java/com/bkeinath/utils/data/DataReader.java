package com.bkeinath.utils.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.bkeinath.utils.excel.ExcelSpreadsheetReader;
import com.bkeinath.utils.excel.domain.ExcelSpreadsheetData;
import com.bkeinath.utils.excel.domain.ExcelSpreadsheetRow;

public class DataReader {

	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(DataReader.class);
	
	public static ExcelSpreadsheetData getSpreadsheetDataFirstPage(InputStream is, String fileName) {
		if (is == null || StringUtils.isBlank(fileName)) {
			return null;
		}
		fileName = fileName.trim().toLowerCase();
		if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
			return ExcelSpreadsheetReader.getSpreadsheetDataFirstPage(is, fileName);
		} else if (fileName.endsWith(".csv")) {
			return parseCsvFile(is);
		}
		return null;
	}
	
	private static ExcelSpreadsheetData parseCsvFile(InputStream is) {
		ExcelSpreadsheetData data = null;
		Scanner scan = null;
		try {
			scan = new Scanner(is);
			data = new ExcelSpreadsheetData();
			List<ExcelSpreadsheetRow> rowList = new ArrayList<>();
			int lineNum = 0;
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (StringUtils.isBlank(line)) {
					continue;
				}
				
				List<String> rowData = Arrays.asList(line.replaceAll("\"", "").split(","));
				if (lineNum == 0) {
					data.setColumnHeaders(rowData);
				} else {
					ExcelSpreadsheetRow row = new ExcelSpreadsheetRow();
					row.setRowData(rowData);
					rowList.add(row);
				}
				lineNum++;
			}
			data.setRowValues(rowList);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("failed to close stream.");
				}
			}
			if (scan != null) {
				scan.close();
			}
		}
		return data;
	}
}

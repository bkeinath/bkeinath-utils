package com.bkeinath.utils.email;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class GmailSender {
	
	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(GmailSender.class);
	
    public static void main(String[] args) throws Exception {
        String from = "sender@gmail.com";
        String pass = "password";
        List<String> to = Arrays.asList("to_person1@gmail.com", "to_person2@hotmail.com" ); // list of recipient email addresses
        String subject = "Java send mail example";
        String body = "Welcome to JavaMail!";

        sendEmail(from, pass, to, subject, body);
        System.out.println("done");
    }
    
	/**	NOTE: Need to be sending FROM a gmail account.
	 *  Also, need to login to the gmail account and go to: https://www.google.com/settings/security/lesssecureapps and set to ON.
	 * 	Click "Manage your Google Account".  Click "Security" on left panel, scroll to bottom and turn "Less secure app access" to ON
	 * 
	 * @param from 
	 * @param pass
	 * @param to
	 * @param subject
	 * @param body
	 * @throws Exception
	 */
    public static void sendGmailEmail(String from, String pass, List<String> to, String subject, String body) throws Exception {
    	sendEmail("smtp.gmail.com", from, pass, to, subject, body);
    }
    
    public static void sendYahooEmail(String from, String pass, List<String> to, String subject, String body) throws Exception {
    	sendEmail("smtp.mail.yahoo.com", from, pass, to, subject, body);
    }
    
    //backwards compatibility
    public static void sendEmail(String from, String pass, List<String> to, String subject, String body) throws Exception {
    	sendEmail("smtp.gmail.com", from, pass, to, subject, body);
    }
    	
    private static void sendEmail(String host, String from, String pass, List<String> to, String subject, String body) throws Exception {
        Properties props = System.getProperties();
        //String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
 
            StringBuilder toAddressList = new StringBuilder();
            toAddressList.append(to.get(0));
            for (int i = 1; i < to.size(); i++) {
            	toAddressList.append(",").append(to.get(i));
            }
            
            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddressList.toString()));
            
            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            log.error("Failed to send email.", e);
            throw new Exception(e);
        }
    }
}

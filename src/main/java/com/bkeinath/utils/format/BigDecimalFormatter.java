package com.bkeinath.utils.format;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalFormatter {

	public static BigDecimal setTwoDecimalDigits(BigDecimal bd) {
		return setDecimalDigits(bd, 2);
	}
	
	public static BigDecimal setDecimalDigits(BigDecimal bd, int numOfDecimalDigits) {
		if (bd == null) {
			return bd;
		}
		return bd.setScale(numOfDecimalDigits, RoundingMode.HALF_UP);
	}
	
	public static String setTwoDecimalDigitsStringNullSafe(BigDecimal bd, String nullValue) {
		return setDecimalDigitsStringNullSafe(bd, 2, nullValue);
	}
	
	public static String setDecimalDigitsStringNullSafe(BigDecimal bd, int numOfDecimalDigits, String nullValue) {
		BigDecimal value = setDecimalDigits(bd, numOfDecimalDigits);
		if (value == null) {
			return nullValue;
		}
		return value.toString();
	}
}

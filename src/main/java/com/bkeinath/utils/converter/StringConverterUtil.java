package com.bkeinath.utils.converter;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.bkeinath.utils.math.MathUtils;
import com.bkeinath.utils.time.TimeUtil;

public class StringConverterUtil {

	public static String getString(String value, String def) {
		if (StringUtils.isBlank(value)) {
			return def;
		}
		return value.trim();
	}
	
	public static boolean getBooleanFalseDefault(String value) {
		return getBoolean(value, false);
	}
	
	public static boolean getBoolean(String value, boolean def) {
		if (StringUtils.isBlank(value)) {
			return def;
		}
		value = value.trim();
		if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("YES") 
				|| value.equalsIgnoreCase("Y") || value.equals("1")) {
			return true;
		}
		return false;
	}
	
	public static BigDecimal getBigDecimalZeroDefault(String value) {
		return getBigDecimal(value, MathUtils.ZERO);
	}
	
	public static BigDecimal getBigDecimal(String value, BigDecimal def) {
		if (StringUtils.isBlank(value)) {
			return def;
		}
		value = value.trim();
		BigDecimal bd = null;
		try {
			bd = new BigDecimal(value);
		} catch (Exception e) {
			bd = def;
		}
		return bd;
	}
	
	public static LocalDate getLocalDateNowDefault(String value) {
		return getLocalDate(value, TimeUtil.getNowEasternLocalDate());
	}

	public static LocalDate getLocalDate(String value, LocalDate def) {
		return TimeUtil.parseLocalDate(value, def);
	}

	public static Integer getIntegerZeroDefault(String value) {
		return getInteger(value, 0);
	}

	public static Integer getInteger(String value, Integer def) {
		if (StringUtils.isBlank(value)) {
			return def;
		}
		value = value.trim();
		Integer i = null;
		try {
			i = Integer.valueOf(value);
		} catch (Exception e) {
			i = def;
		}
		return i;
	}
}

package com.bkeinath.utils.string;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class StringParser {

	public static String BLANK = "";
	public static String SPACE = " ";
	
	public static String getValue(String fullString, String preSubString, String postSubString) {
		return getValue(fullString, preSubString, postSubString, 0, null);
	}
	
	public static String getValue(String fullString, String preSubString) {
		return getValue(fullString, preSubString, BLANK, 0, null);
	}
	
	public static String getValue(String fullString, String prePreStartStr, String preSubString, String postSubString) {
		return getValue(fullString, prePreStartStr, preSubString, postSubString, null);
	}
	
	public static String getValue(String fullString, String prePreStartStr, String preSubString, String postSubString, String postSubStringLimit) {
		List<String> strList = null;
		if (prePreStartStr != null) {
			strList = Arrays.asList(prePreStartStr);
		}
		return getValue(fullString, strList, preSubString, postSubString, postSubStringLimit);
	}
	
	public static String getValue(String fullString, List<String> prePreStartStrList, String preSubString, String postSubString) {
		return getValue(fullString, prePreStartStrList, preSubString, postSubString, null);
	}
	
	public static String getValue(String fullString, List<String> prePreStartStrList, String preSubString, String postSubString, String postSubStringLimit) {
		int startIndex = 0;
		if (prePreStartStrList != null) {
			int index = 0;
			for (String str : prePreStartStrList) {
				index = fullString.indexOf(str, index + 1);
				if (index > 0) {
					startIndex = index + 1;
					if (startIndex >= fullString.length()) {
						startIndex = 0;
					}
				} else {
					//wasn't found. return BLANK;
					return BLANK;
				}
			}
		}
		
		String result = getValue(fullString, preSubString, postSubString, startIndex, postSubStringLimit);
		return result;
	}
	
	/**
	 * Get the substring text that is between preSubString and postSubString/
	 * If you want to the end, set postSubString == "".
	 * NOTE: If can't find the postSubString, then method will return everything to end of string.
	 * 
	 * @param fullString
	 * @param preSubString
	 * @param postSubString
	 * @param startIndexLocation
	 * @return
	 */
	public static String getValue(String fullString, String preSubString, String postSubString, int startIndexLocation, String postSubStringLimit) {
		String value = BLANK;
		if (fullString != null && preSubString != null && postSubString != null) {
			int startValueIndexLocation = 0;
			if (StringUtils.isNotBlank(preSubString)) {
				startValueIndexLocation = fullString.indexOf(preSubString, startIndexLocation);
			}
			if (startValueIndexLocation >= 0) {
				startValueIndexLocation += preSubString.length();
				int endValueIndexLocation = fullString.indexOf(postSubString, startValueIndexLocation);
				
				int endValueLimitIndexLocation = -1;
				if (postSubStringLimit != null) {
					endValueLimitIndexLocation = fullString.indexOf(postSubStringLimit, startIndexLocation);

					if (endValueLimitIndexLocation < 0 && endValueIndexLocation >= 0) {
						//if the endValue limit is -1, but the actual endValue location is not -1, then shouldn't
						//let the failure of the limit result in a blank...
						endValueLimitIndexLocation = endValueIndexLocation;
					}
				}
				
				if (postSubString.equals(BLANK) || postSubString.equals(SPACE) || endValueIndexLocation == -1) {
					//When you want to get everything to the end, use postSubString == "".
					endValueIndexLocation = fullString.length();
				}
				if ((postSubStringLimit != null && endValueLimitIndexLocation >= 0 && endValueIndexLocation >= 0 && endValueLimitIndexLocation >= endValueIndexLocation)
						||
						(postSubStringLimit == null && endValueIndexLocation >= 0) ) {
					value = fullString.substring(startValueIndexLocation, endValueIndexLocation);
				}
			}
		}
		return value;
	}
	
	public static String getValueWithPostSubString(String fullString, String postSubString) {
		if (StringUtils.isBlank(fullString)) {
			return BLANK;
		}
		int postSubStrIndex = fullString.indexOf(postSubString);
		if (postSubStrIndex < 1) {
			return BLANK;
		}
		String result = fullString.trim().substring(0, postSubStrIndex);
		return result;
	}
	
}

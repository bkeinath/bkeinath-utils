package com.bkeinath.utils.time;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeUtil {

	private static final String BLANK = "";
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm");
	private static final java.time.format.DateTimeFormatter JAVA_DATE_TIME_FORMATTER = java.time.format.DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
	public static final DateTimeZone EASTERN = DateTimeZone.forID("America/New_York");
	public static final ZoneId EASTERN_ZONE_ID = ZoneId.of("America/New_York");
	public static final String OPEN_BRACKET = "[";
	public static final long ONE_DAY_MS = 86400000;
	public static final List<LocalDate> HOLIDAYS = Arrays.asList(
			new LocalDate(2021, 1, 18)
			, new LocalDate(2021, 2, 15)
			, new LocalDate(2021, 4, 2)
			, new LocalDate(2022, 4, 15)
			, new LocalDate(2023, 4, 7)
			, new LocalDate(2024, 3, 29)
			, new LocalDate(2025, 4, 18)
			);
	
	public static DateTime getNowEasternDateTime() {
		return DateTime.now(EASTERN);
	}
	
	public static ZonedDateTime getNowEasternZonedDateTime() {
		return ZonedDateTime.now(EASTERN_ZONE_ID);
	}
	
	public static DateTime convert(ZonedDateTime zonedDateTime) {
		if (zonedDateTime == null) {
			return null;
		}
		return DateTime.parse(getZonedDateTimeString(zonedDateTime));
	}
	public static ZonedDateTime convert(DateTime dateTime) {
		if (dateTime == null) {
			return null;
		}
		return parseZonedDateTime(dateTime.toString());
	}
	
	public static ZonedDateTime parseZonedDateTime(String str) {
		if (str == null) {
			return null;
		}
		int openBracket = str.indexOf(OPEN_BRACKET);
		if (openBracket >= 0) {
			str = str.substring(0, openBracket);
		}
		return ZonedDateTime.parse(str);
	}
	
	public static String getDisplayableZonedDateTimeString(ZonedDateTime date) {
		return JAVA_DATE_TIME_FORMATTER.format(date);
	}
	
	public static String getZonedDateTimeString(ZonedDateTime date) {
		if (date == null) {
			return null;
		}
		String s = date.toString();
		int openBracket = s.indexOf(OPEN_BRACKET);
		if (openBracket >= 0) {
			s = s.substring(0, openBracket);
		}
		return s;
	}
	
	public static DateTime getTodayStartOfDayEasternDateTime() {
		return DateTime.now(EASTERN).withTimeAtStartOfDay();
	}
	
	public static DateTime toEasternTime(DateTime time) {
		if (time != null) {
			return time.withZone(EASTERN);
		}
		return time;
	}
	
	public static LocalDate getNowEasternLocalDate() {
		return LocalDate.now(EASTERN);
	}
		
	public static java.time.LocalDate getNowJavaEasternLocalDate() {
		return java.time.LocalDate.now(EASTERN_ZONE_ID);
	}
	
	public static java.time.LocalDate convert(LocalDate date) {
		if (date == null) {
			return null;
		}
		return java.time.LocalDate.parse(date.toString());
	}
	
	public static LocalDate convert (java.time.LocalDate date) {
		if (date == null) {
			return null;
		}
		return LocalDate.parse(date.toString());
	}

	public static LocalDate getBeginningOfYear() {
		return getNowEasternLocalDate().minusDays(getNowEasternLocalDate().getDayOfYear() - 1);
	}

	public static boolean isToday(LocalDate date) {
		if (date == null) {
			return false;
		}
		LocalDate now = getNowEasternLocalDate();
		return now.equals(date);
	}
	
	public static LocalDate parseLocalDate(String dateStr, LocalDate def) {
		if (StringUtils.isBlank(dateStr)) {
			return def;
		}
		dateStr = dateStr.trim();
		LocalDate date = null;
		try {
			dateStr = adjustLocalDateStr(dateStr);
			date = LocalDate.parse(dateStr);
		} catch (Exception e) {
			date = def;
		}
		return date;
	}
	
	private static String adjustLocalDateStr(String dateStr) {
		if (StringUtils.isBlank(dateStr)) {
			return dateStr;
		}
		String dStr = dateStr.trim();
		dStr = dStr.replace("/", "-");
		int firstDash = dStr.indexOf("-");
		//YYYY-MM-DD
		if (firstDash == 4) {
			return dStr;
		}
		//At this point, assuming MM-DD-YYYY
		String[] tokens = dStr.split("-");
		if (tokens.length != 3) {
			return dStr;
		}
		StringBuilder sb = new StringBuilder();
		sb = sb.append(tokens[2]).append("-").append(tokens[0]).append("-").append(tokens[1]);
		return sb.toString();
	}

	public static int getNumberOfDaysFromToday(LocalDate futureDate) {
		return getDaysBetween(getNowEasternLocalDate(), futureDate);
	}
	
	public static int getNumberOfDaysFromToday(LocalDate futureDate, boolean addSameDayDifference) {
		return getDaysBetween(getNowEasternLocalDate(), futureDate, addSameDayDifference);
	}
	
	public static int getDaysBetween(LocalDate start, LocalDate end) {
		return getDaysBetween(start, end, false);
	}
	
	public static int getDaysBetween(LocalDate start, LocalDate end, boolean addSameDayDifference) {
		Days d =  Days.daysBetween(start, end);
		int days = d.getDays();
		if (addSameDayDifference) {
			days++;
		}
		return days;
	}
	
	public static int getDaysBetween(java.time.LocalDate start, java.time.LocalDate end, boolean addSameDayDifference) {
		long d =  DAYS.between(start, end);
		if (addSameDayDifference) {
			d = d + 1;
		}
		return (int) d;
	}
	
	public static int getMinutesBetween(DateTime start, DateTime end) {
		Minutes m =  Minutes.minutesBetween(start, end);
		return m.getMinutes();
	}
	
	public static int getSecondsBetween(DateTime start, DateTime end) {
		Seconds s =  Seconds.secondsBetween(start, end);
		return s.getSeconds();
	}
	
	public static String getDateTimeDisplayString(DateTime dateTime) {
		if (dateTime == null) {
			return BLANK;
		}
		return DATE_TIME_FORMATTER.print(dateTime);
	}
	
	public static LocalDate getThisFriday() {
		return getFriday(getNowEasternLocalDate());
	}
	
	public static LocalDate getFridayAfterDays(int days) {
		return getFriday(getNowEasternLocalDate().plusDays(days));
	}
	
	public static LocalDate getFriday(LocalDate d) {
		if (d == null) {
			d = getNowEasternLocalDate();
		}
		LocalDate friday = d.withDayOfWeek(DateTimeConstants.FRIDAY);
		if (d.getDayOfWeek() > DateTimeConstants.FRIDAY) {
		    friday = friday.plusWeeks(1);
		}
		while (isHoliday(friday)) {
			friday = friday.minusDays(1);
		}
		return friday;
	}
	
	public static LocalDate getThirdFridayOfCurrentMonth() {
		return getThirdFridayOfMonth(getNowEasternLocalDate());
	}
		
	public static LocalDate getThirdFridayOfMonth(LocalDate now) {	
		LocalDate thisMonthExp = getThirdFriday(getNowEasternDateTime());
		if (now.isAfter(thisMonthExp)) {
			//this month exp has passed, so get next months
			thisMonthExp = getThirdFriday(getNowEasternDateTime().plusMonths(1));
		}
		return thisMonthExp;
	}
	
	public static LocalDate getThirdFriday(DateTime dateTime) {
		DateTime dt = dateTime.withDayOfMonth(1).withTimeAtStartOfDay();
		LocalDate ld = dt.toLocalDate();
		for (int i = 0; i < 3; i++) {
			ld = getFriday(ld);
			if (i < 2) {
				ld = ld.plusDays(1);
			}
		}
		return ld;
	}
	
	public static LocalDate getThirdFriday(LocalDate localDate) {
		return getThirdFriday(localDate.toDateTimeAtCurrentTime());
	}
	
	public static Set<LocalDate> getFridaysUntilDate(LocalDate lastDate) {
		Set<LocalDate> set = new HashSet<>();
		if (lastDate == null) {
			return set;
		}
		LocalDate today = getNowEasternLocalDate();
		if (today.isAfter(lastDate)) {
			set.add(getFriday(lastDate));
			return set;
		}

		while (!today.isAfter(lastDate)) {
			set.add(getFriday(today));
			today = today.plusDays(1);
		}
		return set;
	}
	
	public static boolean isMarketClosed() {
		return isMarketClosed(TimeUtil.getNowEasternDateTime());
	}
	
	public static boolean isMarketClosed(DateTime now) {
		DateTime marketOpen = TimeUtil.getNowEasternDateTime().withTime(9, 30, 0, 0);
		DateTime marketClosed = TimeUtil.getNowEasternDateTime().withTime(16, 0, 0, 0);
		if (now.isBefore(marketOpen) || now.isAfter(marketClosed) 
				|| now.getDayOfWeek() == DateTimeConstants.SUNDAY
				|| now.getDayOfWeek() == DateTimeConstants.SATURDAY
				//new years day
				|| now.getDayOfYear() == 1
				//MLK day
				//Presidents Day
				//Memorial Day
				//July 4
				|| (now.getMonthOfYear() == DateTimeConstants.JULY && now.getDayOfMonth() == 4)
				//Labor Day
				//Thanksgiving
				//Christmas
				|| (now.getMonthOfYear() == DateTimeConstants.DECEMBER && now.getDayOfMonth() == 25)
				|| isHoliday(now.toLocalDate())
			) {
			return true;
		}
		return false;
	}
	
	public static LocalDate getLastMarketDate() {
		DateTime now = TimeUtil.getNowEasternDateTime();
		if (!isMarketClosed(now)) {
			return now.toLocalDate();
		}
		now = now.minusDays(1);
		if (now.getDayOfWeek() == DateTimeConstants.SATURDAY 
				|| now.getDayOfWeek() == DateTimeConstants.SUNDAY
			) {
			return TimeUtil.getFriday(now.plusDays(2).toLocalDate()).withDayOfWeek(DateTimeConstants.MONDAY);
		}
		return now.toLocalDate();
	}
	
	public static boolean isHoliday(LocalDate date) {
		return HOLIDAYS.contains(date);
	}
	
	public static boolean isWeekend(LocalDate date) {
		if (date.getDayOfWeek() == DateTimeConstants.SUNDAY
		|| date.getDayOfWeek() == DateTimeConstants.SATURDAY) {
			return true;
		}
		return false;
	}
	
	public static LocalDate getSundayStartOfWeek(LocalDate date) {
		if (date == null) {
			return null;
		}
		if (date.getDayOfWeek() == DateTimeConstants.SUNDAY) {
			return date;
		}
		return date.minusDays(date.getDayOfWeek());
	}
	
	public static LocalDate getSaturdayEndOfWeek(LocalDate date) {
		if (date == null) {
			return null;
		}
		if (date.getDayOfWeek() == DateTimeConstants.SATURDAY) {
			return date;
		}
		LocalDate sundayStartOfWeek = getSundayStartOfWeek(date);
		return sundayStartOfWeek.plusDays(6);
	}
	
	public static Long getOneAmForDate(LocalDate localDate) {
		if (localDate == null) {
			return null;
		}
		return localDate.toDateTimeAtStartOfDay(EASTERN).plusHours(1).getMillis();
	}
		
}
